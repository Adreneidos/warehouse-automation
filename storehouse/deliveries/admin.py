from django.contrib import admin

from storehouse.deliveries.models import Delivery, Provider, ProductInDelivery


class ProductInDeliveryInline(admin.TabularInline):
    model = ProductInDelivery
    extra = 0


class DeliveryAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Delivery._meta.fields]
    inlines = [ProductInDeliveryInline]

    class Meta:
        model = Delivery


admin.site.register(Delivery, DeliveryAdmin)


class ProviderAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Provider._meta.fields]

    class Meta:
        model = Provider


admin.site.register(Provider, ProviderAdmin)


class ProductInDeliveryAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ProductInDelivery._meta.fields]

    class Meta:
        model = ProductInDelivery


admin.site.register(ProductInDelivery, ProductInDeliveryAdmin)