from rest_framework import serializers
from .models import Customer, Provider, Product, Delivery


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Customer

# тестовый класс
class CustomerListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        field = ("second_name", "phone")


class ProviderSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Provider


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Product


class DeliverySerializer(serializers.ModelSerializer):
    #providers = ProviderSerializer(source="provider")

    class Meta:
        model = Delivery
        fields = '__all__'
        depth = 1
        # fields = ['number', 'providers']

